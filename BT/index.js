var arrNhapSo = [];
var themSo = function () {
  var nhapSo = document.getElementById("nhapSo").value * 1;
  arrNhapSo.push(nhapSo);
  document.getElementById("result").innerHTML = `them so : ${arrNhapSo}`;
  //  bài 1 : tính tổng số dương
  function tongSoDuong() {
    var tongSoDuong = 0;
    for (var i = 0; i < arrNhapSo.length; i++) {
      soDuong = arrNhapSo[i];
      if (soDuong > 0) {
        tongSoDuong += soDuong;
      }
    }
    document.getElementById(
      "resultb1"
    ).innerHTML = `Tổng Số Dương: ${tongSoDuong}`;
  }
  document
    .getElementById("btn-tinhTongSoDuong")
    .addEventListener("click", tongSoDuong);

  // bài 2: đếm số dương
  function demSoDuong() {
    var soLuongSoDuong = 0;

    for (var i = 0; i < arrNhapSo.length; i++) {
      soDuong = arrNhapSo[i];
      if (soDuong >= 0) {
        soLuongSoDuong++;
      }
    }
    document.getElementById(
      "resultb2"
    ).innerText = `Số Lượng Số Dương: ${soLuongSoDuong}`;
  }
  document
    .getElementById("btn-demsoduong")
    .addEventListener("click", demSoDuong);

  // bài 3: tìm số nhỏ nhất
  function timSoNhoNhat() {
    var min = arrNhapSo[0];
    for (var i = 0; i < arrNhapSo.length; i++) {
      if (arrNhapSo[i] < min) {
        min = arrNhapSo[i];
      }
    }
    document.getElementById("resultb3").innerHTML = `Số Nhỏ Nhất : ${min}`;
  }
  document
    .getElementById("btn-timSoNhoNhat")
    .addEventListener("click", timSoNhoNhat);

  // bai 4: tìm số Dương nhỏ nhất
  function timSoDuongNhoNhat() {
    var minDuong = arrNhapSo[0];
    for (var i = 0; i < arrNhapSo.length; i++) {
      if (arrNhapSo[i] < minDuong && arrNhapSo[i] > 0) {
        minDuong = arrNhapSo[i];
      }
    }
    document.getElementById(
      "resultb4"
    ).innerHTML = `Số Dương Nhỏ Nhất: ${minDuong}`;
  }
  document
    .getElementById("btn-soDuongNhoNhat")
    .addEventListener("click", timSoDuongNhoNhat);

  // bài 5: tìm số chẵn cuối cùng :
  function soChanCuoiCung() {
    var chanCuoi = 0;
    for (var i = arrNhapSo.length - 1; i >= 0; i--) {
      if (arrNhapSo[i] % 2 == 0) {
        chanCuoi = arrNhapSo[i];

        break;
      }
      return -1;
    }
    document.getElementById(
      "resultb5"
    ).innerText = `Số Chẵn Cuối Cùng: ${chanCuoi}`;
  }
  document
    .getElementById("btn-soChanCuoiCung")
    .addEventListener("click", soChanCuoiCung);

  //  Bài 6: đổi chỗ:
  function doiCho() {
    var viTriNhat = document.getElementById("txtviTriMot").value;
    var viTriHai = document.getElementById("txtviTriHai").value;
    var contentHTML = "";
    // swap
    var temp = viTriNhat;
    temp = arrNhapSo[viTriNhat];
    arrNhapSo[viTriNhat] = arrNhapSo[viTriHai];
    arrNhapSo[viTriHai] = temp;

    for (var index = 0; index < arrNhapSo.length; index++) {
      const item = arrNhapSo[index];
      contentHTML += item;
    }
    document.getElementById("resultb6").innerHTML = contentHTML;
  }
  document.getElementById("btn-doiCho").addEventListener("click", doiCho);
  //  khi user nhập, k biết vị trí bắt đầu từ 0, làm cho chuỗi thứ tự đầu tiên bắt đầu vị trí 1

  // bài 7: Sắp Xếp tăng dần
  function sapXep() {
    var newArray = arrNhapSo.sort();
    console.log("newArray: ", newArray);
    document.getElementById("resultb7").innerHTML = `${arrNhapSo[i]} `;
  }
  document.getElementById("btn-sapXep").addEventListener("click", sapXep);

  // bài 8: tìm sô nguyên tố đầu tiên:
  function soNguyenTo() {
    var soNguyen = 0;

    for (var i = 0; i < arrNhapSo.length; i++) {
      if (arrNhapSo[i] > 1 && arrNhapSo[i] % 2 != 0) {
        soNguyen = arrNhapSo[i];
        break;
      }
    }
    document.getElementById("resultb8").innerHTML = `${soNguyen}`;
    return soNguyen;
  }
  document
    .getElementById("btn-soNguyenToDauTien")
    .addEventListener("click", soNguyenTo);

  // var newArraysoNguyen = [];
  // for (var i = 0; i < arrNhapSo.length; i++) {
  //   if (arrNhapSo[i] > 1) {
  //     newArraysoNguyen.push(arrNhapSo[i]);
  //     // newArraysoNguyen.slice(arrNhapSo[i]);
  //   }
  // }
  // console.log("newArraysoNguyen: ", newArraysoNguyen);
  // bài 10: so sánh số lượng số dương và số lượng số âm, số nào nhiều hơn
  function soSanh() {
    var arrSoLuongSoDuong = [];
    var arrSoLuongSoAm = [];
    for (var i = 0; i < arrNhapSo.length; i++) {
      if (arrNhapSo[i] > 0) {
        arrSoLuongSoDuong.push(arrNhapSo[i]);
      } else {
        arrSoLuongSoAm.push(arrNhapSo[i]);
      }
    }
    if (arrSoLuongSoDuong.length > arrSoLuongSoAm.length) {
      document.getElementById(
        "resultb10"
      ).innerHTML = `Số Lượng Số Dương > Số Lượng Số Âm`;
    } else if (arrSoLuongSoDuong.length > arrSoLuongSoAm.length) {
      document.getElementById(
        "resultb10"
      ).innerHTML = `Số Lượng Số Âm > Số Lượng Số Dương`;
    } else if ((arrSoLuongSoDuong.length = arrSoLuongSoAm.length)) {
      document.getElementById(
        "resultb10"
      ).innerHTML = `Số Lượng Số Âm = Số Lượng Số Dương`;
    }
  }
  document.getElementById("btn-sosanh").addEventListener("click", soSanh);
};

// thêm số
document.getElementById("btn-themSo").addEventListener("click", themSo);

// tìm mảng có số nguyên > 1 trước
